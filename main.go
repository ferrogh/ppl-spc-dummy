package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", Hello)
	http.HandleFunc("/count-interval", countInterval)
	http.HandleFunc("/device-list", deviceList)
	http.HandleFunc("/document-data", documentData)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.ListenAndServe(":"+port, nil)
}

// Hello prints hello world
func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World")
}

func countInterval(w http.ResponseWriter, r *http.Request) {
	dummy := `{
		"cameraCount": 6,
		"places": [
			{
				"placeName": "Stasiun Sudirman",
				"cameras": [
					{
						"id": "A34OE85",
						"cameraName": "Camera North Gate",
						"peopleCount": 752,
						"fluctuate": 20.4
					},
					{
						"id": "A34OE86",
						"cameraName": "Camera South Gate",
						"peopleCount": 852,
						"fluctuate": 8.8
					},
					{
						"id": "A34OE87",
						"cameraName": "Camera Comm A01",
						"peopleCount": 152,
						"fluctuate": 7.2
					}
				]
			},
			{
				"placeName": "Stasiun Haji Nawi",
				"cameras": [
					{
						"id": "A34OE88",
						"cameraName": "Camera Entry Gate",
						"peopleCount": 52,
						"fluctuate": 12.9
					},
					{
						"id": "A34OE89",
						"cameraName": "Camera Comm B01",
						"peopleCount": 1752,
						"fluctuate": 10.7
					},
					{
						"id": "A34OE90",
						"cameraName": "Camera Comm B02",
						"peopleCount": 100,
						"fluctuate": 12.2
					}
				]
			}
		]
	}`
	fmt.Fprintf(w, dummy)
}

func deviceList(w http.ResponseWriter, r *http.Request) {
	dummy := `{
		"deviceListCount": 6,
		"deviceList": [
			 {
				"deviceCode": "A34OE85",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera North Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "752",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE86",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera South Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "852",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE87",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Comm A01",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "152",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE88",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Entry Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Haji Nawi",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "52",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE89",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Comm B01",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Haji Nawi",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "1752",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE90",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Comm B02",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Haji Nawi",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "100",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			}
		]
	}`
	fmt.Fprintf(w, dummy)
}

func documentData(w http.ResponseWriter, r *http.Request) {
	dummy := `{
		"deviceListCount": 5,
		"deviceList": [
			 {
				"deviceCode": "A34OE85",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera North Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "752",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE86",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera South Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "852",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE87",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Comm A01",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Sudirman",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "152",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE88",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Entry Gate",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Haji Nawi",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "52",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			},
			{
				"deviceCode": "A34OE89",
				"deviceImage": "https://pbs.twimg.com/media/ESBVwz9UUAUeSoD?format=png&name=small",
				"deviceName": "Camera Comm B01",
				"deviceSerialNumber": "723EC30894c86250",
				"deviceVersion": "50X20_WL",
				"status": "ON",
				"deviceLocation": "Stasiun Haji Nawi",
				"releaseDate": "2019-10-20 09:44:05",
				"networkMode": "IP",
				"publicPort": "3000",
				"privatePort": "192.168.0.111",
				"peopleCounted": "1752",
				"deviceVideoUrl": "https://www.youtube.com/embed/m384NEQZwfA"
			}
		]
	}`
	fmt.Fprintf(w, dummy)
}
